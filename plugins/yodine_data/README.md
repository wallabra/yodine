# yodine_data

**A Yodine plugin by Gustavo6046**

All the assets, entities and levels that come with Yodine (the game). Note there can be multiple games for Yodine (the engine).

-----

Licensed under MIT.

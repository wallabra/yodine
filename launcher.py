import cProfile, pstats, io
import yodine.launcher as launcher




pr = cProfile.Profile()
pr.enable()

launcher.main()

pr.disable()

# print profile output
s = io.StringIO()
sortby = 'time'
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
print(s.getvalue())
